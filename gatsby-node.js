const Promise = require('bluebird')
const { EPROTO } = require('constants')
const path = require('path')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const blogPost = path.resolve('./src/templates/blog-post.js')
    const projectPage = path.resolve('./src/templates/project.js')
    resolve(
      graphql(
        `
          {
            allContentfulBlogPost {
              edges {
                node {
                  title
                  slug
                }
              }
            }
            allContentfulProject {
              edges {
                node {
                  title
                  slug
                }
              }
            }
          }
        `
      ).then((result) => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }

        const posts = result.data.allContentfulBlogPost.edges
        const projects = result.data.allContentfulProject.edges

        //create posts pages

        posts.forEach((post, index) => {
          createPage({
            path: `/blog/${post.node.slug}/`,
            component: blogPost,
            context: {
              slug: post.node.slug,
            },
          })
        })

        //create projects pages

        projects.forEach((project, index) => {
          createPage({
            path: `/our-projects/${project.node.slug}/`,
            component: projectPage,
            context: {
              slug: project.node.slug,
            },
          })
        })
      })
    )
  })
}
