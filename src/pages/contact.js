import React from 'react'
import Layout from '../components/layout/layout'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'
import { ContactBlock } from '../sections/contact-block/contact-block'

const Contact = () => {
  return (
    <UIContextProvider>
      <Layout>
        <SEO title="Contact-us | Zade" />
        <ContactBlock />
      </Layout>
    </UIContextProvider>
  )
}

export default Contact

export const pageQuery = graphql`
  query ContactQuery {
    site {
      siteMetadata {
        title
        url
      }
    }
  }
`
