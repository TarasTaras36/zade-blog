import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout/layout'
import { ArticlesBlock } from '../sections/articles-block/articles-block'

import { FilterContext, FilterContextProvider } from '../context/filter.context'

import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'

const Blog = (props) => {
  const posts = props?.data?.allContentfulBlogPost.edges

  return (
    <UIContextProvider>
      <FilterContextProvider>
        <SEO title="Blog | Zade" />
        <Layout location={''}>
          <ArticlesBlock articles={posts} title="Read our news" />
        </Layout>
      </FilterContextProvider>
    </UIContextProvider>
  )
}

export default Blog

export const pageQuery = graphql`
  query BlogIndexQuery {
    site {
      siteMetadata {
        title
        url
      }
    }
    allContentfulBlogPost(limit: 10) {
      edges {
        node {
          title
          category
          slug
          publishDate(formatString: "DD.MM.YY")
          heroImage {
            fluid {
              base64
              # tracedSVG
              srcWebp
              srcSetWebp
              src
            }
          }
        }
      }
    }
  }
`
