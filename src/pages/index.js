import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout/layout'
import '../styles/fonts.css'
import { ProjectsBlock } from '../sections/projects-block/projects-block'
import { ArticlesBlock } from '../sections/articles-block/articles-block'
import { FilterContextProvider } from '../context/filter.context'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'
import { HomeHero } from '../sections/home-hero/home-hero'
import { InitLoader } from '../components/init-loader/init-loader'

const RootIndex = (props) => {
  const heroData = props?.data?.contentfulHomeHero
  const projectsData = props?.data?.allContentfulProject.edges
  const posts = props?.data?.allContentfulBlogPost.edges

  return (
    <UIContextProvider>
      <FilterContextProvider>
        <SEO title="Home | Zade" />
        <Layout location={props.location}>
          <HomeHero heroData={heroData} />
          <ProjectsBlock
            projectsData={projectsData}
            title="Things we are proud of"
            seeMore
            itemsToShow={3}
            showPagination={false}
          />
          {/* separate */}
          <div style={{ height: 97, width: '100%' }} />
          <ArticlesBlock
            seeMore
            itemsToShow={3}
            articles={posts}
            title={'Our latest updates'}
            showPagination={false}
          />
        </Layout>
      </FilterContextProvider>
    </UIContextProvider>
  )
}

export default RootIndex

export const pageQuery = graphql`
  query HomeQuery {
    site {
      siteMetadata {
        title
        url
      }
    }

    contentfulHomeHero {
      tag
      title
      description
      heroMedia {
        file {
          url
        }
        fixed(quality: 100, width: 1200) {
          src
          base64
        }
      }
    }

    allContentfulProject(filter: { showOnHomePage: { eq: true } }, limit: 3) {
      edges {
        node {
          title
          tags
          slug
          category
          previewDescription
          previewImages {
            file {
              url
            }
            fluid(quality: 100) {
              src
              base64
              srcSet
            }
          }
          mobileImage {
            fluid(quality: 100) {
              src
              base64
              srcSet
            }
          }
        }
      }
    }
    allContentfulBlogPost(filter: { showOnHomePage: { eq: true } }, limit: 3) {
      edges {
        node {
          title
          slug
          publishDate(formatString: "DD.MM.YY")
          heroImage {
            fluid(quality: 100) {
              base64
              # tracedSVG
              src
              base64
              srcSet
            }
          }
        }
      }
    }
  }
`
