import React from 'react'
import Layout from '../components/layout/layout'
import { HeroBlock } from '../sections/hero-block/hero-block'
import { graphql } from 'gatsby'
import { TeamBlock } from '../sections/team-block/team-block'
import { CapabilitiesBlock } from '../sections/capabilities-block/capabilities-block'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'

const AboutUs = (props) => {
  const heroData = props?.data?.contentfulAboutUsHero
  const teamData = props?.data?.allContentfulTeamMembers.edges
  const capabilitiesData = props?.data.contentfulSection.body

  return (
    <UIContextProvider>
      <Layout>
        <SEO title="About us | Zade" />
        <HeroBlock heroData={heroData} />
        <CapabilitiesBlock capabilitiesData={capabilitiesData} />
        <TeamBlock teamData={teamData} />
      </Layout>
    </UIContextProvider>
  )
}

export default AboutUs

export const pageQuery = graphql`
  query AboutUsPageQuery {
    site {
      siteMetadata {
        title
        url
      }
    }
    contentfulAboutUsHero {
      title
      heroImage {
        fixed(quality: 100, width: 1200) {
          base64
          srcWebp
          srcSetWebp
        }
      }
    }

    allContentfulTeamMembers {
      edges {
        node {
          name
          surname
          position
          image {
            fluid {
              base64
              # tracedSVG
              srcWebp
              srcSetWebp
              sizes
            }
          }
        }
      }
    }
    contentfulSection(page: { eq: "about-us" }) {
      body {
        json
      }
    }
  }
`
