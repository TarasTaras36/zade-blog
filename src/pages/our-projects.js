import React from 'react'
import Layout from '../components/layout/layout'
import { Link, graphql } from 'gatsby'
import { ProjectsBlock } from '../sections/projects-block/projects-block'
import { FilterContextProvider } from '../context/filter.context'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'

const OurProjects = (props) => {
  const projectsData = props?.data?.allContentfulProject.edges
  const location = props.location.pathname

  return (
    <UIContextProvider>
      <FilterContextProvider>
        <SEO title="Works | Zade" />
        <Layout>
          <ProjectsBlock
            projectsData={projectsData}
            title="Featured works"
            itemsToShow={6}
            location={location}
          />
        </Layout>
      </FilterContextProvider>
    </UIContextProvider>
  )
}

export default OurProjects

export const pageQuery = graphql`
  query ProjectsQuery {
    site {
      siteMetadata {
        title
        url
      }
    }
    allContentfulProject {
      edges {
        node {
          title
          tags
          category
          previewDescription
          slug
          previewImages {
            file {
              url
            }
            fluid {
              base64
              # tracedSVG
              srcWebp
              srcSetWebp
            }
          }
          mobileImage {
            fluid(quality: 100) {
              src
              base64
              srcSet
            }
          }
        }
      }
    }
  }
`
