import React, { createContext, useState, useEffect } from 'react'
import useIntersect from '../hooks/intersect-hook'
/*
 * Ui context peovider
 */

export const UIContext = createContext()

/**
 * Light theme
 */
const lightTheme = {
  theme: 'light',
  '--color-primary': 'black',
  '--color-background': 'white',
}
/**
 * Dark theme
 */

const darkTheme = {
  theme: 'dark',
  '--color-primary': '#FCDFDF',
  '--color-background': '#0e0e0e',
}

const UIContextProvider = ({ children }) => {
  const [theme, setTheme] = useState({})
  const [ref, entry] = useIntersect({ rootMargin: '-50%' })
  const [resize, setResize] = useState(false)

  const [showModal, setShowModal] = useState(false)

  /*
   * handle Theme
   */

  useEffect(() => {
    entry.isIntersecting ? setTheme(darkTheme) : setTheme(lightTheme)
    Object.keys(theme)
      .filter((key) => key !== 'theme')
      .map((key) => {
        const value = theme[key]
        document.documentElement.style.setProperty(key, value)
      })
  }, [entry, theme])

  return (
    <UIContext.Provider
      value={{
        theme,
        ref,
        setTheme,
        setResize,
        resize,
        showModal,
        setShowModal,
      }}
    >
      {children}
    </UIContext.Provider>
  )
}

export { UIContextProvider }
