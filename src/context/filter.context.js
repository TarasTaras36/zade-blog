import { createContext } from 'react'
import React, { useReducer } from 'react'

export const filterReducer = (state, { type, payload }) => {
  switch (type) {
    case 'SET_FILTER': {
      return { ...state, currentFilter: payload }
    }
    case 'SET_PROJECTS': {
      return { ...state, projects: payload }
    }
    case 'SET_POSTS': {
      return { ...state, posts: payload }
    }

    default:
      return state
  }
}

export const initState = {
  posts: [],
  projects: [],
  currentFilter: 'all',
}

export const FilterContext = createContext()

const FilterContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(filterReducer, initState)

  const setCurrentFilter = (filter) => {
    dispatch({ type: 'SET_FILTER', payload: filter })
  }

  // useEffect(() => {
  //   if (state.currentFilter !== 'all') {
  //     dispatch({
  //       type: 'SET_PROJECS',
  //       payload: state.projects.filter(
  //         (article) => article.node.category === state.currentFilter
  //       ),
  //     })
  //   } else {

  //   }
  // }, [currentFilter])

  return (
    <FilterContext.Provider
      value={{
        setCurrentFilter,
        currentFilter: state.currentFilter,
        posts: state.posts,
        projects: state.projects,
      }}
    >
      {children}
    </FilterContext.Provider>
  )
}

export { FilterContextProvider }
