import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import React from 'react'
import { useRichTextOptions } from '../../hooks/rich-text-options-hook'
import { Banner } from '../banner/banner'
import * as styles from './rich-text.module.scss'

const RichText = ({ content }) => {
  const { richTextOptions } = useRichTextOptions({
    imageClassName: styles.image,
    paragraphClassName: styles.paragraph,
    h2ClassName: styles.h2,
    h3ClassName: styles.h3,
  })
  const Block = documentToReactComponents(content, richTextOptions)
  return (
    <div className={styles.content}>
      <Banner />
      {Block}
    </div>
  )
}

export { RichText }
