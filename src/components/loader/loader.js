import React from 'react'
import './loader.css'
import classNames from 'class-names'

const Loader = (className) => {
  return <div className={classNames('loader', className)}>Loading...</div>
}

export { Loader }
