import { hidden } from 'chalk'
import React from 'react'
import * as styles from './container.module.scss'

export const Container = ({ children }) => {
  return (
    <div style={{ overflow: 'hidden' }}>
      <div className={styles.container}>{children}</div>
    </div>
  )
}
