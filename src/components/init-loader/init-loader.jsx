import React from 'react'
import * as styles from './init-loader.module.scss'

const InitLoader = () => {
  return <div className={styles.loader}></div>
}

export { InitLoader }
