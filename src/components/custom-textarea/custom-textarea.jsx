import React, { useState } from 'react'
import * as styles from './custom-textarea.module.scss'
import classNames from 'class-names'

/**
 * Renders CustomTextarea
 */
const CustomTextarea = ({ className, label, name, onChange, value }) => {
  const [focused, setFocused] = useState(false)
  return (
    <div className={classNames(styles.customTextarea, className)}>
      <label
        className={classNames(styles.label, {
          [styles.focusedLabel]: value || focused,
          [styles.unfocused]: !value && !focused,
        })}
      >
        {label}
      </label>
      <textarea
        className={styles.textArea}
        onChange={(e) => onChange(e, name)}
      ></textarea>
    </div>
  )
}

export { CustomTextarea }
