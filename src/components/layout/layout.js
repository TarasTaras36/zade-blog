import React, { useContext, useLayoutEffect, useRef, useState } from 'react'
import './base.css'
import { Container } from '../container'
import { Header } from '../header/header'
import '../../styles/fonts.css'
import { Footer } from '../footer/footer'
import { useScroll } from './useScroll'
import { animated, config } from 'react-spring'
import { UIContext } from '../../context/ui.context'
import { GetQuoteModal } from '../get-quuote-modal/get-quote-modal'

const Template = ({ children }) => {
  const { interpolatePage } = useScroll(ref)
  const [contentHeight, setContentHeight] = useState(null)
  const ref = useRef()

  React.useEffect(() => {
    setContentHeight(ref.current.clientHeight)
  })

  const { showModal } = useContext(UIContext)

  React.useEffect(() => {
    setTimeout(() => {
      if (!!ref) {
        setContentHeight(ref.current?.clientHeight)
      }
    }, 1500)
  }, [])

  return (
    <div style={{ minHeight: contentHeight }}>
      {showModal && <GetQuoteModal />}
      <Header />
      <div className="scroll-wrap">
        <div ref={ref} className="content">
          <animated.div
            style={{
              transform: interpolatePage,
            }}
          >
            <Container>
              {/* <div className={'grainy-background'} /> */}
              {children}

            </Container>
            <Footer />
          </animated.div>
        </div>
      </div>
    </div>
  )
}

export default Template
