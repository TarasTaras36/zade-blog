import { useLayoutEffect } from 'react'
import { useCallback } from 'react'

import { useSpring, config } from 'react-spring'

const useScroll = (ref) => {
  const [{ x }, set] = useSpring(() => ({ x: 0 }))

  const onScroll = useCallback(
    (e) => {
      set({ x: window.scrollY })
    },
    [x]
  )
  useLayoutEffect(() => {
    addEventListener('scroll', () => onScroll())
  }, [ref])
  const interpolatePage = x.interpolate((x) => `translate3d(0,-${x}px,0)`)
  return { onScroll, interpolatePage }
}

export { useScroll }
