import React, { useContext } from 'react'
import * as styles from './article-preview.module.scss'
import Img from 'gatsby-image'
import { Link } from 'gatsby'
import { H2 } from '../../typography/typography'
import { UIContext } from '../../context/ui.context'

const ArticlePreview = ({
  article: { heroImage, title, publishDate, slug },
}) => {
  return (
    <Link className={styles.container} to={`/blog/${slug}`}>
      <div className={styles.article}>
        <Img className={styles.articleImage} fluid={heroImage.fluid} />

        <div className={styles.articleInfo}>
          <img
            className={styles.arrow}
            src={require('../../assets/icons/arrow.svg')}
          />
          <H2 className={styles.articleTitle}>{title}</H2>
          <div className={styles.articleDate}>WORK: {publishDate}</div>
        </div>
      </div>
    </Link>
  )
}

export { ArticlePreview }
