import React from 'react'
import MarkDown from 'react-markdown'
import * as styles from './custom-block.module.scss'
import classNames from 'class-names'

const CustomBlock = ({ children, titleButtonHash, titleOnly }) => {
  return (
    <MarkDown
      className={classNames(styles.block, {
        [styles.titleButtonHash]: titleButtonHash,
        [styles.titleOnly]: titleOnly,
      })}
    >
      {children}
    </MarkDown>
  )
}

export { CustomBlock }
