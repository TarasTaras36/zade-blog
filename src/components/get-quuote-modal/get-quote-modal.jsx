import React, { useContext, useState } from 'react'
import * as styles from './get-quote-modal.module.scss'
import { useForm } from 'react-hook-form'
import classNames from 'class-names'
import { sendForm } from './form-service'
import { UIContext } from '../../context/ui.context'

/**
 * Input
 */
const Input = ({ label, register, required }) => {
  return (
    <div className={styles.inputWrap}>
      <label className={styles.label}>{label}</label>
      <input
        className={styles.input}
        name={label}
        ref={register({ required })}
      />
    </div>
  )
}

const GetQuoteModal = () => {
  const { register, handleSubmit, watch, errors } = useForm()
  const [sent, setSent] = useState(false)
  const { setShowModal } = useContext(UIContext)

  const send = (data) => {
    try {
      sendForm(data)
      setSent(true)
      setTimeout(() => {
        setShowModal(false)
      }, 2000)
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <div className={styles.overlay}>
      <div className={styles.getQuoteModal}>
        {sent ? (
          <div className={styles.successAlert}>
            Thank hor get in touch, we will contact you ASAP !
          </div>
        ) : (
          <>
            <img
              className={styles.close}
              src={require('../../assets/icons/close.svg')}
              onClick={() => setShowModal(false)}
            />
            <div className={styles.title}>Get a quote</div>
            <div className={styles.caption}>
              Describe the project that the developers will be working on:
            </div>
            <form
              className={styles.form}
              onSubmit={handleSubmit((data) => send(data))}
            >
              <Input label="Name" register={register} required />
              <Input label="Email" register={register} required />
              <div className={styles.inputWrap}>
                <label className={styles.label}>Message</label>
                <textarea
                  className={classNames(styles.input, styles.textarea)}
                  name="message"
                  ref={register}
                />
              </div>
              {/* <div className={styles.checkbox}>
            <input type="checkbox" />
            <span className={styles.checkboxCheckmark}></span>
            <label>I’d like to sign an NDA with Zade Agency Software</label>
          </div> */}

              <div className={styles.container}>
                <input
                  className={styles.submit}
                  type="submit"
                  value="GET A QUOTE"
                />
              </div>
            </form>
          </>
        )}
      </div>
    </div>
  )
}

export { GetQuoteModal }
