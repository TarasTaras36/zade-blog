import facebook from '../../assets/icons/facebook.png';
import linkedin from '../../assets/icons/linkedIN.png';
import twitter from '../../assets/icons/twitter.png';

const useFooterData = () => {
  const navigation = {
    mainLink: [
      { name: 'Works', to: '/our-projects' },
      { name: 'About us', to: '/about-us' },
      { name: 'Blog', to: '/blog' },
      { name: 'Services', to: '/services' },
      { name: 'Work with us', to: '/work-with-us' }
    ],
    subLink: [
      { name: 'Strategy', to: '/strategy' },
      { name: 'Branding and Identity', to: '/branding-and-identity' },
      { name: 'UX Consulting', to: '/ux-consulting' },
      { name: 'Mobile Apps Design', to: '/mobile-apps-design' },
      { name: 'Web Apps Design', to: '/web-apps-design' },
      { name: 'Startups', to: '/startups' },
      { name: 'Websites', to: '/websites' },
    ],
    socialLink: [
      {
        src: facebook,
        to: 'https://www.facebook.com/Zade-Agency-103966894698153'
      },
      {
        src: linkedin,
        to: 'https://www.linkedin.com/company/zade-agency/'
      }, {
        src: twitter,
        to: 'https://twitter.com/zade_agency'
      }
    ]
  }

  return { navigation }
}
export { useFooterData }