import { Link } from 'gatsby'
import React from 'react'
import { useFooterData } from './footer.hook'
import * as styles from './footer.module.scss'
import logo from '../../assets/zade-logo.png'
import classNames from 'classnames';

const Footer = () => {
  const { navigation } = useFooterData()
  return (
    <footer className={styles.footer}>
      <div className={styles.container}>
        <div className={styles.title}>
          <img className={styles.logo} src={logo} />
          <text>A studio dedicated to functional and visual solutions</text>
          <div className={styles.copyright}>© Copyright Zade 2021</div>
        </div>
        <div className={styles.nav}>
          {navigation.mainLink.map(({ name, to }) => (
            <Link className={styles.mainLink} key={to} to={to}>{name}</Link>
          ))}
        </div>
        <div className={classNames(styles.nav, styles.subNav)}>
          {navigation.subLink.map(({ name, to }) => (
            <Link className={styles.subLink} key={to} to={to}>{name}</Link>
          ))}
        </div>
        <div className={styles.contact}>
          <div className={styles.contactInfo}>
            <text>
              Email: hello@zade.agency
          </text>
            <text>
              Project team: +38 097 649 70 67
          </text>
            <text>
              HR team: +38 097 480 79 94
          </text>
          </div>
          <div className={styles.location}>
            Europe, Ukraine <br />Lviv, Zelena 81 Street, <br />79017
        </div>

        </div>
        <div className={styles.socialNetwork}>
          {navigation.socialLink.map(({ src, to }) => (
            <a href={to}><img src={src} /></a>
          ))}
        </div>
      </div>
    </footer>
  )
}

export { Footer }
