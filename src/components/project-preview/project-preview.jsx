import React, { useContext, useEffect, useState } from 'react'
import * as styles from './project-preview.module.scss'
import Image from 'gatsby-image'
import { H2, Paragraph } from '../../typography/typography'
import { ButtonLink } from '../button-link/button-link'
import { FilterContext } from '../../context/filter.context'
import useIntersect from '../../hooks/intersect-hook'
import classNames from 'class-names'
import { Link } from 'gatsby'
import { useMediaPoints } from '../../hooks/media-points.hook'
import { Video } from '../video/video'

const ProjectPreview = ({ project: { node: data } }) => {
  const {
    tags,
    title,
    previewImages,
    previewDescription,
    slug,
    category,
    mobileImage,
  } = data

  const { setCurrentFilter } = useContext(FilterContext)

  const [ref, node] = useIntersect({ rootMargin: '-35%' })
  const [animate, setAnimate] = useState(false)

  const { mobile } = useMediaPoints()

  const handleClick = (category) => {
    window.scrollTo(0, 0)
    setCurrentFilter(category)
  }

  useEffect(() => {
    node.isIntersecting && setAnimate(true)
  }, [node])
  return (
    <div className={styles.projectPreview} ref={ref}>
      <div className={styles.description}>
        <div className={styles.tags}>
          <div className={styles.tag} onClick={() => handleClick(category)}>
            #{category}
          </div>
          {/* {tags.map((tag) => (
            <div
              onClick={() => setCurrentFilter(tag)}
              className={styles.tag}
              key={tag}
            >
              #{tag}
            </div>
          ))} */}
        </div>
        <Link to={`/our-projects/${slug}`}>
          <H2 className={styles.descriptionTitle}>{title}</H2>
          <Paragraph className={styles.descriptionText}>
            {previewDescription}
          </Paragraph>
        </Link>
        <ButtonLink
          to={`/our-projects/${slug}`}
          className={styles.descriptionLink}
        >
          learn more
        </ButtonLink>
      </div>
      {mobile ? (
        <div
          className={classNames(styles.wrapper, {
            [styles.animate]: animate,
          })}
        >
          <Image className={styles.image} fluid={mobileImage?.fluid} />
        </div>
      ) : (
        previewImages.map((source, i) => {
          const isImage = !!source.fluid
          return (
            <div
              key={i}
              className={classNames(styles.wrapper, {
                [styles.animate]: animate,
                [styles.singleWrapper]: previewImages.length === 1,
              })}
            >
              {!isImage && (
                <Video
                  videoSrcURL={source.file.url}
                  variant={classNames(styles.image, {
                    [styles.singleImage]: previewImages.length === 1,
                  })}
                />
              )}
              <Image
                key={i}
                className={classNames(styles.image, {
                  [styles.singleImage]: previewImages.length === 1,
                })}
                fluid={source?.fluid}
              />
            </div>
          )
        })
      )}
    </div>
  )
}

export { ProjectPreview }
