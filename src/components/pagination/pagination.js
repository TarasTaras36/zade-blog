import React, { useState, useEffect } from 'react'
import * as styles from './pagination.module.scss'
import { scrollToRef } from '../../hooks/scroll-to'

const Pagination = ({
  data,
  currentIndex,
  className,
  itemsToShow,
  setSlice,
  scrollTo,
}) => {
  const [activePage, setActivePage] = useState(1)
  const itemsToDisplay = data.length / itemsToShow + 1
  const pages = data.filter((el, i) => i + 1 < itemsToDisplay)

  /**
   * Scroll to top on page change
   */
  useEffect(() => {
    scrollToRef(scrollTo)
  }, [activePage])

  /**
   * Previous page
   */
  const prev = () => {
    setSlice((prev) => ({
      from: prev.from - itemsToShow,
      to: prev.to - itemsToShow,
    }))
    setActivePage((prev) => prev - 1)
  }

  /**
   * Next page
   */
  const next = () => {
    setSlice((prev) => ({
      from: prev.from + itemsToShow,
      to: prev.to + itemsToShow,
    }))
    setActivePage((prev) => prev + 1)
  }

  /**
   * Jump to page
   */
  const jump = (page) => {
    setSlice({
      from: page * itemsToShow - itemsToShow,
      to: page * itemsToShow,
    })
    setActivePage(page)
  }

  return (
    <div className={className}>
      <div onClick={() => prev()}>{'<'}</div>
      <div className={styles.numbers}>
        {pages.map((el, i) => (
          <div
            key={i}
            className={activePage === i + 1 && styles.active}
            onClick={() => jump(i + 1)}
          >
            {i + 1}
          </div>
        ))}
      </div>
      <div onClick={() => next()}>{'>'}</div>
    </div>
  )
}

export { Pagination }
