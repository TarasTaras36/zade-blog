import React, { useContext } from 'react'
import { UIContext } from '../../context/ui.context'

import * as styles from './banner.module.scss'

const Banner = () => {
  const { setShowModal } = useContext(UIContext)
  return (
    <div className={styles.banner}>
      <div className={styles.title}>
        Hand-selected developers to fit your needs at scale
      </div>
      <div className={styles.hr} />
      <div className={styles.resources}>
        <div className={styles.resourcesItem}>
          <div className={styles.resourcesTitle}>Reviewed on</div>
          <img src={require('../../assets/icons/clutch.svg')} />
        </div>
        <div className={styles.resourcesItem}>
          <img src={require('../../assets/icons/stars-red.svg')} alt="stars" />
          <div className={styles.resourcesReview}>17 Review</div>
        </div>
        {/* <div className={styles.resourcesItem}>
          <img
            src={require('../../assets/icons/stars-yellow.svg')}
            alt="stars"
          />
          <img
            className={styles.resourcesFirms}
            src={require('../../assets/icons/good-firms.svg')}
          />
        </div> */}
      </div>
      <div className={styles.caption}>
        We delivered 200+ software projects worldwide
      </div>
      <div className={styles.subcaption}>We offer:</div>
      <ul>
        <li>• Web & Mobile apps development</li>
        <li>• Dedicated software developers</li>
        <li>• Cybersecurity services</li>
      </ul>
      <button className={styles.button} onClick={() => setShowModal(true)}>
        GET A QUOTE
      </button>
    </div>
  )
}

export { Banner }
