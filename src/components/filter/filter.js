import React, { useContext } from 'react'
import * as styles from './filter.module.scss'
import { H2 } from '../../typography/typography'
import { FilterContext } from '../../context/filter.context'
import { ButtonLink } from '../button-link/button-link'
import classNames from 'class-names'

const CategoryFilter = ({ categories, className }) => {
  const { setCurrentFilter, currentFilter } = useContext(FilterContext)
  const uniqueFilters = categories.reduce(
    (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
    []
  )
  return (
    <>
      <div className={classNames(styles.filters, className)}>
        <span
          className={classNames(styles.item, {
            [styles.selected]: currentFilter === 'all',
          })}
          onClick={() => setCurrentFilter('all')}
        >
          #all
        </span>
        {uniqueFilters.map((filter) => (
          <div
            className={classNames(styles.item, {
              [styles.selected]: currentFilter === filter,
            })}
            key={filter}
            onClick={() => setCurrentFilter(filter)}
          >
            #{filter}
          </div>
        ))}
      </div>
    </>
  )
}

export { CategoryFilter }
