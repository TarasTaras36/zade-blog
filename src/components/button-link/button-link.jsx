import React from 'react'
import { Link } from 'gatsby'
import classNames from 'class-names'
import * as styles from './button-link.module.scss'

const ButtonLink = ({ children, className, onClick, to }) => {
  return (
    <Link to={to} className={classNames(styles.buttonLink, className)}>
      {children}
    </Link>
  )
}

export { ButtonLink }
