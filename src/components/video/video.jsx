import React from 'react'

const Video = ({ videoSrcURL, variant }) => {
  return (
    <video
      className={variant}
      autoPlay
      playsInline
      muted
      loop
      width="100%"
      height="100%"
    >
      <source src={videoSrcURL} type="video/mp4" />
    </video>
  )
}

export { Video }
