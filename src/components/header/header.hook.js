const useHeaderData = () => {
  const navigation = [
    { name: 'Works', to: '/our-projects' },
    { name: 'About us', to: '/about-us' },
    { name: 'Blog', to: '/blog' },
    // { name: 'Contact', to: '/contact' },
  ]
  return { navigation }
}

export { useHeaderData }
