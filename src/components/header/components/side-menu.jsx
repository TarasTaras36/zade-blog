import { Link } from 'gatsby'
import React from 'react'
import { useHeaderData } from '../header.hook'
import * as styles from './side-menu.module.scss'
import classNames from 'class-names'
import { DDStatus } from '../header'

const SideMenu = ({ isShown, setDropDownSatus }) => {
  const { navigation } = useHeaderData()
  return (
    <div
      className={classNames(styles.dropDown, {
        [styles.animate]: isShown === DDStatus.show,
        [styles.hide]: isShown === DDStatus.hide,
      })}
      //style={{ display: isShown === DDStatus.hide ? 'block' : 'none' }}
    >
      <div className={styles.header}>
        <Link to={'/'} className={styles.logo}>
          <img src={require('../../../assets/zade-pink.svg')} />
        </Link>
        <div
          className={styles.button}
          onClick={() => setDropDownSatus(DDStatus.hide)}
        >
          BACK
        </div>
      </div>
      <div className={styles.linkList}>
        {navigation.map((link) => (
          <Link className={styles.linkListItem} key={link.to} to={link.to}>
            {link.name}
          </Link>
        ))}
      </div>
      <div className={styles.copyright}>© COPYRIGHT ZADE 2020</div>
    </div>
  )
}

export { SideMenu }
