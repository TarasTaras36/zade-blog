import React, { useContext, useState } from 'react'
import { Link } from 'gatsby'
import { useHeaderData } from './header.hook'
import * as styles from './header.module.scss'
import { SideMenu } from './components/side-menu'
import classNames from 'class-names'
import { useLocation } from '@reach/router'
import { UIContext } from '../../context/ui.context'
import { GetQuoteModal } from '../get-quuote-modal/get-quote-modal'

export enum DDStatus {
  init = 'init',
  show = 'show',
  hide = 'hide',
}

const Header = () => {
  const { pathname } = useLocation()
  const { navigation } = useHeaderData()
  const [dropdownStatus, setDropDownSatus] = useState(DDStatus.init)
  const { theme, setShowModal } = useContext(UIContext)

  return (
    <div className={styles.headerWrapper}>
      <div className={styles.header}>
        <Link className={styles.logo} to="/">
          <img
            src={
              theme.theme === 'dark'
                ? require('../../assets/zade-pink.svg')
                : require('../../assets/zade-black.svg')
            }
          />
        </Link>

        <div className={styles.navigation}>
          {navigation.map(({ name, to }) => (
            <Link
              key={to}
              className={classNames(styles.link, {
                [styles.active]: pathname.includes(to),
              })}
              to={to}
            >
              {name}
            </Link>
          ))}
        </div>

        <div className={styles.work} onClick={() => setShowModal(true)}>
          Work with us
        </div>
        <div className={styles.burger}>
          {/* <img
            src={require('../../assets/icons/header-menu.svg')}
            width={'25px'}
            onClick={() => setIsShowen(!isShown)}
          /> */}
          <div
            className={styles.menu}
            onClick={() =>
              setDropDownSatus(
                dropdownStatus === DDStatus.show ? DDStatus.hide : DDStatus.show
              )
            }
          >
            MENU
          </div>
          {/* <button
            className={classNames(styles.hamburger, styles.hamburgerSpring, {
              [styles.isActive]: dropdownStatus === DDStatus.show,
            })}
            type="button"
            onClick={() =>
              setDropDownSatus(
                dropdownStatus === DDStatus.show ? DDStatus.hide : DDStatus.show
              )
            }
          >
            <span className={styles.hamburgerBox}>
              <span className={styles.hamburgerInner}></span>
            </span>
          </button> */}
        </div>
      </div>
      <SideMenu isShown={dropdownStatus} setDropDownSatus={setDropDownSatus} />
    </div>
  )
}

export { Header }
