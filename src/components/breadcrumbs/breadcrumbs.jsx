import { Link } from 'gatsby'
import React from 'react'
import * as styles from './breadcrumbs.module.scss'
import classNames from 'class-names'

const BreadCrumbs = ({ pathname, to }) => {
  return (
    <div className={styles.breadcrumbs}>
      <Link className={styles.link} to="/">
        Home{' '}
      </Link>
      <Link to={`/${to}`} className={classNames(styles.bradcrumb)}>
        {` > `} {pathname}
      </Link>
    </div>
  )
}

export { BreadCrumbs }
