import React, { useState } from 'react'
import * as styles from './custom-input.module.scss'
import classNames from 'class-names'

const CustomInput = ({ className, label, name, onChange, value }) => {
  const [focused, setFocused] = useState(false)
  return (
    <div className={classNames(styles.inputContainer, className)}>
      <label
        className={classNames(styles.label, {
          [styles.focusedLabel]: value || focused,
          [styles.unfocused]: !value && !focused,
        })}
      >
        {label}
      </label>
      <input
        className={classNames(styles.input, {
          [styles.focused]: focused || value,
        })}
        onChange={(e) => onChange(e, name)}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
      />
    </div>
  )
}

export { CustomInput }
