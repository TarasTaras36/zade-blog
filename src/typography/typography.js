import React from 'react'
import * as styles from './typography.module.scss'
import classNames from 'class-names'

const Title = ({ children, className }) => {
  return (
    <div className={classNames(styles.pageTitle, className)}>{children}</div>
  )
}

const Paragraph = ({ children, className }) => {
  return (
    <div className={classNames(styles.paragraph, className)}>{children}</div>
  )
}
const H1 = ({ children, className }) => {
  return <div className={classNames(styles.h1, className)}>{children}</div>
}
const H2 = ({ children, className }) => {
  return <div className={classNames(styles.h2, className)}>{children}</div>
}
const H3 = ({ children, className }) => {
  return <div className={classNames(styles.h3, className)}>{children}</div>
}

const HR = ({ className }) => {
  return <div className={classNames(styles.hr, className)} />
}

const Bold = ({ className }) => {
  return <div className={classNames(styles.bold, className)}>{children}</div>
}

const LI = ({ children }) => {
  return <div className={styles.li}>{children}</div>
}
const UL = ({ children }) => {
  return <div className={styles.ul}>{children}</div>
}

export { H1, H2, H3, HR, Title, Bold, Paragraph, LI, UL }
