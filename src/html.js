import React from 'react'
import PropTypes from 'prop-types'
import { InitLoader } from './components/init-loader/init-loader'

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />

        <link rel="preconnect" href="https://fonts.gstatic.com"></link>
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap"
          rel="stylesheet"
        ></link>
        <script
          type="application/javascript"
          src="https://widget.clutch.co/static/js/widget.js"
        ></script>

        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        <InitLoader />
        {props.preBodyComponents}
        <div
          style={{ display: 'none' }}
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
        {/* <script
          type="text/javascript"
          src="https://widget.clutch.co/static/js/widget.js"
        ></script>{' '} */}
        {/* <div
          className="clutch-widget"
          data-url="https://widget.clutch.co"
          data-widget-type="2"
          data-height="50"
          data-clutchcompany-id="890022"
          style={{ position: 'fixed', bottom: 0, right: 0 }}
        ></div> */}
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
