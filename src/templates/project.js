import React from 'react'
import Layout from '../components/layout/layout'
import { graphql } from 'gatsby'
import { useRichTextOptions } from '../hooks/rich-text-options-hook'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import * as styles from './project.module.scss'
import { HR, Title } from '../typography/typography'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'
import { BreadCrumbs } from '../components/breadcrumbs/breadcrumbs'
import { Video } from '../components/video/video'
import Image from 'gatsby-image'
import { RichText } from '../components/rich-text/rich-text'

const Project = ({ data: { contentfulProject }, location }) => {
  if (!contentfulProject) return null
  const {
    body: { json: richText },
    title,
    previewDescription,
    heroMedia,
  } = contentfulProject

  const showImage = !!heroMedia?.fixed

  return (
    <UIContextProvider>
      <Layout>
        <SEO title={`${title} | Zade`} description={previewDescription} />
        <BreadCrumbs pathname="Projects" to="our-projects" />
        <Title className={styles.title}>{title}</Title>
        <HR className={styles.hr} />
        {showImage ? (
          <Image className={styles.heroImage} fixed={heroMedia?.fixed} />
        ) : (
          <Video videoSrcURL={heroMedia.file.url} variant={styles.heroImage} />
        )}
        <RichText content={richText} />
      </Layout>
    </UIContextProvider>
  )
}

export default Project

export const pageQuery = graphql`
  query ProjectBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    contentfulProject(slug: { eq: $slug }) {
      title
      previewDescription
      heroMedia {
        file {
          url
        }
        fixed(quality: 100) {
          src
          base64
          srcSet
        }
      }
      body {
        json
      }
    }
  }
`
