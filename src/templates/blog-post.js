import React, { useContext } from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout/layout'
import { H1, HR } from '../typography/typography'
import * as styles from './blog-post.module.scss'
import SEO from '../components/SEO/seo'
import { UIContextProvider } from '../context/ui.context'
import { BreadCrumbs } from '../components/breadcrumbs/breadcrumbs'
import Image from 'gatsby-image'
import { Video } from '../components/video/video'
import { RichText } from '../components/rich-text/rich-text'

const BlogPostTemplate = (props) => {
  const {
    title,
    author,
    publishDate,
    socials,
    authorComment: { authorComment },
    authorPosition,
    heroImage,
    authorPhoto,
  } = props?.data?.contentfulBlogPost
  const { json: richText } = props?.data?.contentfulBlogPost?.content

  const showImage = !!heroImage?.fluid

  return (
    <UIContextProvider>
      <Layout location={props.location}>
        <SEO title={`${title} | Zade`} />
        <BreadCrumbs pathname="Blog" to="blog" />
        <H1 className={styles.title}>{title}</H1>
        <HR className={styles.hr} />
        {showImage ? (
          <Image
            className={styles.heroImage}
            fluid={heroImage?.fluid}
            imgStyle={{ objectFit: 'contain' }}
          />
        ) : (
          <Video videoSrcURL={heroImage.file.url} variant={styles.heroImage} />
        )}
        <RichText content={richText} />
        <div className={styles.meta}>
          <Image fluid={authorPhoto.fluid} className={styles.metaPhoto} />
          <div className={styles.descriptions}>
            <div>Written by</div>
            <div className={styles.metaName}>{author}</div>
            <div className={styles.metaPosition}>{authorPosition}</div>
            <div className={styles.metaComment}>{authorComment}</div>
            <div className={styles.metaSocial}>
              <a href={socials[0]}>
                <img src={require('../assets/icons/instagram.svg')} />
              </a>
              <a href={socials[1]}>
                <img src={require('../assets/icons/twitter.svg')} />
              </a>
              {/* <a href=''>
                <img src={require('../assets/icons/instagram.svg')} />
              </a> */}
            </div>
          </div>
        </div>
      </Layout>
    </UIContextProvider>
  )
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    contentfulBlogPost(slug: { eq: $slug }) {
      title
      heroImage {
        fluid(maxWidth: 1180, background: "rgb:000000") {
          ...GatsbyContentfulFluid_withWebp
        }
      }
      author
      socials
      authorComment {
        authorComment
      }
      authorPosition
      authorPhoto {
        fluid(maxWidth: 80, quality: 100) {
          ...GatsbyContentfulFluid_withWebp
        }
      }
      publishDate(formatString: "MMMM DD, YYYY")
      content {
        json
      }
    }
  }
`
