enum CustomBlockTypes {
  onlyTitle = 'Title only',
  titleText = 'Title with text',
  titleButtonHash = 'Title with button and hashtags',
}

export { CustomBlockTypes }
