import React, {
  useContext,
  useEffect,
  useReducer,
  useRef,
  useState,
} from 'react'
import * as styles from './projects-block.module.scss'
import { Title } from '../../typography/typography'
import { Link } from 'gatsby'
import { ProjectPreview } from '../../components/project-preview/project-preview'
import {
  FilterContext,
  filterReducer,
  initState,
} from '../../context/filter.context'
import { Pagination } from '../../components/pagination/pagination'
import { ButtonLink } from '../../components/button-link/button-link'
import { UIContext } from '../../context/ui.context'
import { CategoryFilter } from '../../components/filter/filter'
import { useProjectsData } from './projects-block.hook'

const ProjectsBlock = ({
  projectsData,
  title,
  seeMore,
  itemsToShow,
  showPagination = true,
  location,
}) => {
  // const [filteredProjects, setFilteredProjects] = useState(projectsData)
  const [slice, setSlice] = useState({
    from: 0,
    to: itemsToShow,
  })
  const { categories, sortedProjects } = useProjectsData(projectsData, slice)

  const ref = useRef()
  const { ref: intersectRef } = useContext(UIContext)

  return (
    <section className={styles.projectsBlock} ref={intersectRef}>
      <div className={styles.heading}>
        <Title className={styles.title}>{title}</Title>
        {seeMore ? (
          <ButtonLink to="/our-projects" className={styles.link}>
            View all
          </ButtonLink>
        ) : //  <CategoryFilter className={styles.filters} categories={categories} />
        null}
      </div>
      {sortedProjects.map((project) => (
        <ProjectPreview key={project.node.title} project={project} />
      ))}
      {showPagination && sortedProjects.length > 4 && (
        <Pagination
          scrollTo={ref}
          className={styles.pagination}
          data={sortedProjects}
          itemsToShow={itemsToShow}
          setSlice={setSlice}
        />
      )}
    </section>
  )
}

export { ProjectsBlock }
