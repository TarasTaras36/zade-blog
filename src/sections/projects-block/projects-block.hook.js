import { useContext, useEffect, useReducer, useState } from 'react'
import {
  FilterContext,
  filterReducer,
  initState,
} from '../../context/filter.context'
/*
 * Projects data
 */

const useProjectsData = (projectsData, slice) => {
  const { currentFilter } = useContext(FilterContext)
  const [state, dispatch] = useReducer(filterReducer, initState)
  /**
   * Display projects
   */
  const [projectsToDisplay, setProjects] = useState(projectsData)

  /*
   * Categories
   */
  const categories = projectsData.map((el) => el.node.category)

  const sortedProjects = projectsToDisplay.slice(slice.from, slice.to)

  useEffect(() => {
    dispatch({ type: 'SET_PROJECTS', payload: projectsData })
  }, [])

  useEffect(() => {
    if (currentFilter && currentFilter !== 'all') {
      setProjects(
        projectsData.filter(
          (article) => article.node.category === currentFilter
        )
      )
    } else {
      setProjects(projectsData)
    }
  }, [currentFilter])
  return {
    categories,
    sortedProjects,
  }
}

export { useProjectsData }
