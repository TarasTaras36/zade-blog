import React from 'react'
import * as styles from './home-hero.module.scss'
import Image from 'gatsby-image'
import { Video } from '../../components/video/video'

const HomeTitle = ({ tag, title, description }) => {
  return (
    <div className={styles.title}>
      <div className={styles.tag}>{tag}</div>
      <div className={styles.caption}>{title}</div>
      <div className={styles.description}>{description}</div>
    </div>
  )
}

const HomeHero = ({ heroData }) => {
  const { tag, title, description, heroMedia } = heroData

  const showImage = !!heroMedia?.fixed

  return (
    <section className={styles.homeHero}>
      <HomeTitle tag={tag} title={title} description={description} />
      {showImage ? (
        <Image className={styles.heroImage} fixed={heroImage?.fixed} />
      ) : (
        <Video videoSrcURL={heroMedia.file.url} variant={styles.heroImage} />
      )}
    </section>
  )
}

export { HomeHero }
