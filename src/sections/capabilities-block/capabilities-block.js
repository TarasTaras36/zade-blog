import React from 'react'
import { useRichTextOptions } from '../../hooks/rich-text-options-hook'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import * as styles from './capabilities.module.scss'

const CapabilitiesBlock = ({ capabilitiesData }) => {
  const { richTextOptions } = useRichTextOptions({
    listClassName: styles.list,
    h2ClassName: styles.title,
    paragraphClassName: styles.paragraph,
  })
  const content = documentToReactComponents(
    capabilitiesData.json,
    richTextOptions
  )
  return <section className={styles.capabilitiesBlock}>{content}</section>
}

export { CapabilitiesBlock }
