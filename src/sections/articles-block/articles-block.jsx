import { Link } from 'gatsby'
import React, { useEffect, useState, useContext } from 'react'
import { ArticlePreview } from '../../components/article-preview/article-preview'
import { ButtonLink } from '../../components/button-link/button-link'
import { CategoryFilter } from '../../components/filter/filter'
import { Pagination } from '../../components/pagination/pagination'
import { FilterContext } from '../../context/filter.context'
import { UIContext } from '../../context/ui.context'
import { Title } from '../../typography/typography'
import * as styles from './articles-block.module.scss'

const ArticlesBlock = ({
  articles,
  title,
  seeMore,
  showPagination = true,
  itemsToShow = 3,
}) => {
  const [articlesToShow, setArticlesToShow] = useState(articles)

  const [slice, setSlice] = useState({
    from: 0,
    to: itemsToShow,
  })

  const sortedArr = articlesToShow.slice(slice.from, slice.to)
  const categories = articles.map((el) => el.node.category)
  const { currentFilter } = useContext(FilterContext)

  //Refactor

  useEffect(() => {
    if (currentFilter && currentFilter !== 'all') {
      setArticlesToShow(
        articles.filter((project) => project.node.category === currentFilter)
      )
    } else {
      setArticlesToShow(articles)
    }
  }, [currentFilter])

  return (
    <section className={styles.articleBlock}>
      <div className={styles.heading}>
        <Title className={styles.title}>{title}</Title>
        {seeMore ? (
          <ButtonLink to="/blog" className={styles.link}>
            VIEW ALL
          </ButtonLink>
        ) : (
          <CategoryFilter className={styles.filters} categories={categories} />
        )}
      </div>
      {sortedArr.map((article) => (
        <ArticlePreview key={article.node.title} article={article.node} />
      ))}
      {showPagination && articlesToShow > 4 && (
        <Pagination
          itemsToShow={itemsToShow}
          data={articlesToShow}
          setSlice={setSlice}
          className={styles.pagination}
        />
      )}
    </section>
  )
}

export { ArticlesBlock }
