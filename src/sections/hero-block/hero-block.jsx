import React from 'react'
import * as styles from './hero-block.module.scss'
import { H1, HR, Title } from '../../typography/typography'
import Image from 'gatsby-image'
import { ButtonLink } from '../../components/button-link/button-link'
import classNames from 'class-names'

const HeroBlock = ({ heroData }) => {
  const { title, heroImage } = heroData

  return (
    <section className={styles.hero}>
      <Title className={styles.heroTitle}>{title}</Title>
      <HR className={styles.hr} />
      <Image className={styles.heroImage} fixed={heroImage?.fixed} />
    </section>
  )
}

export { HeroBlock }
