import React, { useState, useRef, useEffect } from 'react'
import { CustomInput } from '../../components/custom-input/custom-input'
import { CustomTextarea } from '../../components/custom-textarea/custom-textarea'
import { Title, H1, H3, HR } from '../../typography/typography'
import * as styles from './contact.module.scss'
import classNames from 'class-names'
import { sendForm } from './form.service'
import { Loader } from '../../components/loader/loader'

const interests = [
  '#mobileapp',
  '#webapp',
  '#responsive',
  '#design',
  '#creative',
  '#ecomerce',
]

const ContactBlock = () => {
  const [values, setValues] = useState({
    name: '',
    email: '',
    message: '',
    selectedInterests: [],
  })
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [loaderPosition, setLoaderPosition] = useState({ x: 0, y: 0 })
  const ref = useRef(null)

  const handleSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)
    const response = await sendForm(values)
    setLoading(false)
    setSuccess(true)
    await setTimeout(() => setSuccess(false), 3000)
  }
  const handleInputChange = (e, key) => {
    const value = e.target.value
    setValues((prev) => ({
      ...prev,
      [key]: value,
    }))
  }

  /**
   * Set interests
   */
  const handleInterests = (interest) => {
    values.selectedInterests.includes(interest)
      ? setValues({
          ...values,
          selectedInterests: values.selectedInterests.filter(
            (el) => el !== interest
          ),
        })
      : setValues({
          ...values,
          selectedInterests: [...values.selectedInterests, interest],
        })
  }

  /**
   * Loader position
   */
  useEffect(() => {
    if (loading) {
      const el = ref?.current.getBoundingClientRect()
      setLoaderPosition({
        x: el.x,
        y: el.y,
      })
    }
  }, [loading])

  return (
    <section className={styles.contactBlock}>
      <div className={styles.contactInfo}>
        <H3 className={styles.subtitle}>ALL OTHER ENQUIRES</H3>
        <ul className={styles.list}>
          <li>
            <a href="">contact@zade.agency</a>
          </li>
          <li>
            <a href="">jobs@zade.agency</a>
          </li>
          <li>
            <a href="">hello@zade.agency</a>
          </li>
        </ul>
        <H3 className={styles.subtitle}>VISIT US</H3>
        <ul className={styles.list}>
          <li>Europe, Ukraine</li>
          <li>Lviv, Zelena 81 Street</li>
          <li>79017</li>
        </ul>
      </div>
      <form className={styles.form}>
        <CustomInput
          className={styles.input}
          label="Name"
          name="name"
          onChange={handleInputChange}
          value={values.name}
        />
        <CustomInput
          className={styles.input}
          label="Email"
          name="email"
          onChange={handleInputChange}
          value={values.email}
        />
        <CustomTextarea
          className={styles.textarea}
          label="Tell us your story"
          name="message"
          onChange={handleInputChange}
          value={values.message}
        />
        <div className={styles.caption}>Wat you are looking for? </div>
        <div className={styles.interests}>
          {interests.map((el, i) => (
            <div
              key={i}
              className={classNames(styles.item, {
                [styles.active]: values.selectedInterests.includes(el),
              })}
              onClick={() => handleInterests(el)}
            >
              {el}
            </div>
          ))}
        </div>
        <div className={styles.submit}>
          <button
            className={styles.button}
            type="submit"
            onClick={(e) => handleSubmit(e)}
          >
            Send
          </button>
          <div ref={ref}>{loading && <Loader />}</div>
          {success && (
            <div
              style={{ top: loaderPosition.y, left: loaderPosition.x }}
              className={classNames(styles.alert, 'grainy-background')}
            >
              Email sent !
            </div>
          )}
        </div>
      </form>
    </section>
  )
}

export { ContactBlock }
