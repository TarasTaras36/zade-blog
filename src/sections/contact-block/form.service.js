import axios from 'axios'
/**
 * Form service
 */

const sendForm = async (data) => {
  const interests = data.selectedInterests.join(',')
  const message = {
    subject: 'Zade | agency',
    text: 'Hello',
    html: `<p>${data.name}</p><p>${data.email}</p><p>${data.message}</p><p>${interests}</p>`,
  }
  try {
    const response = await axios.post(
      'https://z22jqg40rc.execute-api.us-east-2.amazonaws.com/latest/email',
      { message }
    )
    return { response }
  } catch (err) {
    console.log(err)
  }
}

export { sendForm }
