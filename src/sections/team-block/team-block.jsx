import React, { useContext } from 'react'
import * as styles from './team-block.module.scss'
import { Title } from '../../typography/typography'
import Image from 'gatsby-image'
import { UIContext } from '../../context/ui.context'

const TeamMate = ({ member: { name, surname, image, position } }) => {
  if (!image) return null
  return (
    <div className={styles.teamMate}>
      <Image className={styles.photo} fluid={image?.fluid} />
      <div className={styles.name}>
        {name} {surname}
      </div>
      <div className={styles.position}>{position}</div>
    </div>
  )
}

const TeamBlock = ({ teamData }) => {
  const { ref } = useContext(UIContext)
  return (
    <section className={styles.teamBlock} ref={ref}>
      <Title className={styles.title}>Our team composition</Title>
      <div className={styles.list}>
        {teamData.map(({ node }, i) => (
          <TeamMate key={i} member={node} />
        ))}
      </div>
    </section>
  )
}

export { TeamBlock }
