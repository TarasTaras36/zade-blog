import { graphql, useStaticQuery } from 'gatsby'

export const useContentfulImage = (assetUrl) => {
  const { allContentfulAsset } = useStaticQuery(
    graphql`
      query CONTENTFUL_IMAGE_QUERY {
        allContentfulAsset {
          nodes {
            file {
              url
              details {
                image {
                  height
                  width
                }
              }
            }
            fixed(quality: 100) {
              # ...GatsbyContentfulFlui_withWebp
              base64
              srcWebp
              srcSetWebp
            }
          }
        }
      }
    `
  )
  return {
    image: allContentfulAsset.nodes.find((n) => n.file.url === assetUrl),
  }
}
