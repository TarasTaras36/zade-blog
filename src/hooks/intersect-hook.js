import { useEffect, useRef, useState } from 'react'

export default ({ root = null, rootMargin, threshold = 0 }) => {
  const [entry, updateEntry] = useState({})
  const [node, setNode] = useState(null)

  useEffect(() => {
    if (window === undefined) return
    const observer = new window.IntersectionObserver(
      ([entry]) => updateEntry(entry),
      {
        root,
        rootMargin,
        threshold,
      }
    )

    observer.disconnect()

    if (node) observer.observe(node)

    return () => observer.disconnect()
  }, [node])

  return [setNode, entry]
}
