import React, { useContext } from 'react'
import { BLOCKS, MARKS } from '@contentful/rich-text-types'
import { Paragraph, H2, H3, Bold, LI, UL } from '../typography/typography'
import { CustomBlock } from '../components/custom-block/custom-block'
import { useContentfulImage } from '../hooks/contentful-image-hook'
import Img from 'gatsby-image'
import { CustomBlockTypes } from '../models/enums'
import { Video } from '../components/video/video'
import { UIContext } from '../context/ui.context'

const useRichTextOptions = ({
  listClassName,
  h1ClassName,
  h2ClassName,
  h3ClassName,
  imageClassName,
  paragraphClassName,
}) => {
  const { setResize, resize } = useContext(UIContext)

  const richTextOptions = {
    renderNode: {
      [BLOCKS.PARAGRAPH]: (node, children) => (
        <Paragraph className={paragraphClassName}>{children}</Paragraph>
      ),
      [BLOCKS.HEADING_1]: (node, children) => (
        <H2 className={h2ClassName}>{children}</H2>
      ),
      [BLOCKS.HEADING_2]: (node, children) => (
        <H3 className={h3ClassName}>{children}</H3>
      ),
      [BLOCKS.UL_LIST]: (node, children) => (
        <UL className={listClassName}>{children} </UL>
      ),
      [BLOCKS.LIST_ITEM]: (node, children) => <LI>{children}</LI>,

      [MARKS.BOLD]: (node, children) => <Bold>{children}</Bold>,

      [BLOCKS.EMBEDDED_ENTRY]: (node, children) => {
        switch (node?.data?.target?.fields?.type['en-US']) {
          // add any cases and customize blocks
          case CustomBlockTypes.titleButtonHash:
            return (
              <CustomBlock titleButtonHash>
                {node.data.target.fields.text['en-US']}
              </CustomBlock>
            )
          case CustomBlockTypes.onlyTitle:
            return (
              <CustomBlock titleOnly>
                {node.data.target.fields.text['en-US']}
              </CustomBlock>
            )
          default:
            return (
              <CustomBlock>{node.data.target.fields.text['en-US']}</CustomBlock>
            )
        }
      },
      [BLOCKS.EMBEDDED_ASSET]: (node, children) => {
        const asset = useContentfulImage(
          node.data.target.fields.file['en-US'].url
        )
        const isImage = !!asset.image.fixed
        return isImage ? (
          <Img
            className={imageClassName}
            title={node.data.target.fields.title['en-US']}
            fixed={asset.image.fixed}
            onLoad={() => setResize(!resize)}
          />
        ) : (
          <Video
            videoSrcURL={asset.image.file.url}
            className={imageClassName}
          />
        )
      },
    },
  }

  return { richTextOptions }
}

export { useRichTextOptions }
