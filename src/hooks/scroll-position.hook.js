const { useCallback, useEffect, useState } = require('react')

/**
 * Scroll position hook
 */

const useScrollPosition = () => {
  let pos
  useEffect(() => {
    window.addEventListener('scroll', (e) => {
      pos = window.pageYOffset
    })
  })
  return pos
}

export { useScrollPosition }
