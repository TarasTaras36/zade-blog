/*
 * Scroll to position
 */

const scrollToRef = (ref) => {
  if (!ref) return
  // ref.current.scrollIntoView()
  window.scrollTo(0, 0)
}

export { scrollToRef }
