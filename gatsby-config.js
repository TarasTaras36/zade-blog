const path = require('path')
require('dotenv').config()

const contentfulConfig = {
  spaceId: process.env.CONTENTFUL_SPACE_ID,
  accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
}

module.exports = {
  siteMetadata: {
    title: 'Zade Agency',
    titleTemplate: 'Zade',
    description:
      'We are a team of technologists who want to make the world better by delivering great software that users love 🖤 . Zade Agency is a combination of years of experience and fresh thinking to help our clients digitally transform their businesses.',
    url: 'https://www.zade-blog.com',
    image: 'thumbnail.png',
  },
  pathPrefix: '/gatsby-contentful-starter',
  plugins: [
    'gatsby-transformer-remark',
    'gatsby-transformer-sharp',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sharp',
    'gatsby-plugin-sass',
    '@contentful/rich-text-html-renderer',
    '@contentful/rich-text-react-renderer',
    'gatsby-transformer-ffmpeg',

    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        '@components': path.join(__dirname, 'src/components'),
        '@pages': path.join(__dirname, 'src/pages'),
        '@assets': path.join(__dirname, 'src/assets'),
        '@core.scss': path.join(__dirname, 'src/styles/core.scss'),
        '@templates': path.join(__dirname, 'src/templates'),
        '@typography': path.join(__dirname, 'scr/typography'),
      },
    },
    {
      resolve: 'gatsby-source-contentful',
      options: {
        spaceId: 'nlwxqm6gylgf',
        accessToken: 'TEvqqTlSwYXBWjMqBMQwz0lQb2XM1gHsN43l8lQXwSA',
      },
    },
    {
      resolve: 'gatsby-plugin-segment-analytics',
      options: {
        writeKey: 'JZEHFWJQnsDnyzZnHF23wSmeucF2xkjV',
        eventName: 'YOUR PAGE EVENT NAME',
      },
    },
  ],
}
